using Album.Models;
using Album.Models.Repositories;

namespace Album.Services;

public class TagService
{
    private readonly TagRepository _tagRepository;

    public TagService(TagRepository tagRepository)
    {
        _tagRepository = tagRepository;
    }

    public Tag? FindTagById(int tagId)
    {
        return _tagRepository.FindById(tagId);
    }

    public Tag Save(Tag tag)
    {
        return _tagRepository.Save(tag);
    }

    public void Delete(Tag tag)
    {
        _tagRepository.Delete(tag);
    }
}