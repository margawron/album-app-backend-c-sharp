using Album.Models;
using Album.Models.Repositories;

namespace Album.Services;

public class BinaryDataService
{
    private readonly BinaryDataRepository _binaryDataRepository;

    public BinaryDataService(BinaryDataRepository binaryDataRepository)
    {
        _binaryDataRepository = binaryDataRepository;
    }

    public BinaryData Save(BinaryData binaryData)
    {
        return _binaryDataRepository.Save(binaryData);
    }

    public BinaryData? FindById(int binaryDataId)
    {
        return _binaryDataRepository.FindById(binaryDataId);
    }
}