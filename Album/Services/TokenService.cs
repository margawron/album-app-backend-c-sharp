using Album.Dtos;
using Album.Models;
using Album.Settings;
using JWT;
using JWT.Algorithms;
using JWT.Serializers;

namespace Album.Services;

public class TokenService
{
    private readonly UserService _userService;
    private readonly AppSettings _appSettings;
    private readonly IJsonSerializer _serializer;
    private readonly IJwtEncoder _encoder;
    private readonly IJwtDecoder _decoder;
    public TokenService(UserService userService, AppSettings appSettings)
    {
        _userService = userService;
        _appSettings = appSettings;
        IJwtAlgorithm algorithm = new HMACSHA256Algorithm();
        IBase64UrlEncoder urlEncoder = new JwtBase64UrlEncoder();
        _serializer = new JsonNetSerializer();
        _encoder = new JwtEncoder(algorithm, _serializer, urlEncoder);
        _decoder = new JwtDecoder(_serializer, urlEncoder);
    }

    public TokenResponse GenerateTokenForUser(User user)
    {
        var expirationDate = DateTime.Now + TimeSpan.FromSeconds(_appSettings.ExpirationTime);
        var secondsSinceEpoch = UnixEpoch.GetSecondsSince(expirationDate);
        var payload = new Dictionary<string, object>
        {
            {"id", user.Id},
            {"username", user.Username },
            {"role", user.UserRole },
            {"creationDate", user.CreationDate.ToUnixTimeMilliseconds() },
            {"accountState", user.AccountState },
            {"deactivationDate", user.DeactivationDate},
            {"exp", secondsSinceEpoch}
        };
        var token = _encoder.Encode(payload, _appSettings.Secret);
        return new TokenResponse
        {
            Token = token,
            ExpirationDate = expirationDate,
            TokenType = "Bearer"
        };
    }

    public User? GetUserFromToken(string token)
    {
        var json = _decoder.Decode(token, _appSettings.Secret, true);
        var payloadDictionary = _serializer.Deserialize<Dictionary<string, object>>(json);
        var userId = Int32.Parse(payloadDictionary["id"].ToString() ?? string.Empty);
        return _userService.FindUserById(userId);
    }
}