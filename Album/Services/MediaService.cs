using Album.Models;
using Album.Models.Repositories;

namespace Album.Services;

public class MediaService
{
    private readonly MediaRepository _mediaRepository;

    public MediaService(MediaRepository mediaRepository)
    {
        _mediaRepository = mediaRepository;
    }

    public Media? FindById(int mediaId)
    {
        return _mediaRepository.FindById(mediaId);
    }

    public Media Create(Media media)
    {
        return _mediaRepository.Create(media);
    }

    public Media Update(Media media)
    {
        return _mediaRepository.Update(media);
    }

    public void Delete(Media media)
    {
        _mediaRepository.Delete(media);
    }
}