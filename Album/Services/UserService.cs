using Album.Dtos;
using Album.Exceptions;
using Album.Models;
using Album.Models.Repositories;
using NodaTime;

namespace Album.Services;

public class UserService
{
    private readonly UserRepository _userRepository;

    public UserService(UserRepository userRepository)
    {
        _userRepository = userRepository;
    }

    public User FindAndCheckUserCredentials(string username, string password)
    {
        var user = _userRepository.FindByUsername(username);
        if (user == null)
        {
            throw new CodeException(UserErrorCode.UserNotExists);
        }
        if (BCrypt.Net.BCrypt.Verify(password, user.PasswordHash))
        {
            return user;
        }
        throw new CodeException(UserErrorCode.InvalidPassword);
    }

    public User? FindUserById(int id)
    {
        return _userRepository.FindUserById(id);
    }

    public User CreateUser(RegisterRequest registerRequest)
    {
        var hashPassword = BCrypt.Net.BCrypt.HashPassword(registerRequest.Password);
        var user = new User
        {
            Username = registerRequest.Login,
            PasswordHash = hashPassword,
            Email = registerRequest.Email,
            AccountState = UserAccountState.Normal,
            CreationDate = SystemClock.Instance.GetCurrentInstant(),
            DeactivationDate = null,
            UserRole = UserRole.User
        };
        return _userRepository.Create(user);
    }

    public void ValidateIfExistsUserWithNameOrEmail(string registerRequestLogin, string registerRequestEmail)
    {
        var user = _userRepository.FindByUsername(registerRequestLogin) ??
                   _userRepository.FindByEmail(registerRequestEmail);

        if (user != null)
        {
            throw new CodeException(UserErrorCode.UserExists);
        }
    }

    public List<User> GetAllUsers()
    {
        return _userRepository.FindAll();
    }
}