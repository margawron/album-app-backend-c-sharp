namespace Album.Dtos;

public class TokenResponse
{
    public string Token { get; set; }
    public string TokenType { get; set; }
    public DateTime ExpirationDate { get; set; }
}