using Album.Models;

namespace Album.Dtos.Domain;

public class TagDto
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int OwnerId { get; set; }
    public List<int> Medias { get; set; }

    public static TagDto fromTag(Tag tag)
    {
        return new TagDto
        {
            Id = tag.Id,
            Name = tag.Name,
            OwnerId = tag.OwnerId,
            Medias = tag.Medias?.Select(x => x.Id).ToList() ?? new List<int>()
        };
    }
}