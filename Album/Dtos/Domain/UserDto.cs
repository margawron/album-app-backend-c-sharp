using Album.Models;
using NodaTime;

namespace Album.Dtos.Domain;

public class UserDto
{
    public int Id { get; set; }
    public string Username { get; set; }
    public string Email { get; set; }
    public string Role { get; set; }
    public Instant CreationDate { get; set; }
    public UserAccountState UserAccountState { get; set; }
    public Instant? DeactivationDate { get; set; }
    public List<int> Media { get; set; }
    public List<int> Tags { get; set; }
    
    public static UserDto fromEntity(User user)
    {
        return new UserDto
        {
            Id = user.Id,
            Username = user.Username,
            Email = user.Email,
            Role = user.UserRole,
            CreationDate = user.CreationDate,
            UserAccountState = user.AccountState,
            DeactivationDate = user.DeactivationDate,
            Media = user.Medias?.Select(x => x.Id).ToList() ?? new List<int>(),
            Tags = user.Tags?.Select(x => x.Id).ToList() ?? new List<int>()
        };
    }
}