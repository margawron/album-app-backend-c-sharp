using Album.Models;

namespace Album.Dtos.Domain;

public class MediaDto
{
    public int Id { get; set; }
    public string MimeType { get; set;}
    public string Name { get; set; }
    public int OwnerId { get; set; }
    public List<int> Tags { get; set; }

    public static MediaDto fromMedia(Media media)
    {
        return new MediaDto
        {
            Id = media.Id,
            MimeType = media.MimeType,
            Name = media.Name,
            OwnerId = media.UserId,
            Tags = media.Tags?.Select(x => x.Id).ToList() ?? new List<int>()
        };
    }
}