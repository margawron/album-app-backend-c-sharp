namespace Album.Dtos;

public class ErrorDto
{
    public String ErrorCode { get; set; }
    public String ErrorMessage { get; set; }
}