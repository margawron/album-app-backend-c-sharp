namespace Album.Dtos;

public class TagCreateRequest
{
    public string TagName { get; set; }
}