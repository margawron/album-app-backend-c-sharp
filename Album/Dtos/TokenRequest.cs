using System.ComponentModel.DataAnnotations;

namespace Album.Dtos;

public class TokenRequest
{
    [Required]
    public String Username { get; set; }
    [Required]
    public String Password { get; set; }
}