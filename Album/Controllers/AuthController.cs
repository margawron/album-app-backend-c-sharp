using Album.Dtos;
using Album.Dtos.Domain;
using Album.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Album.Controllers;

[Authorize]
[ApiController]
[Route("auth")]
public class AuthController : ControllerBase
{
    private readonly UserService _userService;
    private readonly TokenService _tokenService;

    public AuthController(UserService userService, TokenService tokenService)
    {
        _userService = userService;
        _tokenService = tokenService;
    }

    [AllowAnonymous]
    [HttpPost("login")]
    public TokenResponse Login([FromBody] TokenRequest tokenRequest)
    {
        var user = _userService.FindAndCheckUserCredentials(tokenRequest.Username, tokenRequest.Password);
        return _tokenService.GenerateTokenForUser(user);
    }

    [AllowAnonymous]
    [HttpPost("register")]
    public UserDto Register([FromBody] RegisterRequest registerRequest)
    {
        _userService.ValidateIfExistsUserWithNameOrEmail(registerRequest.Login, registerRequest.Email);
        var createdUser = _userService.CreateUser(registerRequest);
        return UserDto.fromEntity(createdUser);
    }
}