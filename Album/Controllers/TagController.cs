using Album.Dtos;
using Album.Dtos.Domain;
using Album.Exceptions;
using Album.Extensions;
using Album.Models;
using Album.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Album.Controllers;

[Authorize]
[ApiController]
[Route("tag")]
public class TagController : ControllerBase
{
    private readonly TagService _tagService;
    private readonly UserService _userService;

    public TagController(TagService tagService, UserService userService)
    {
        _tagService = tagService;
        _userService = userService;
    }

    [HttpGet("{tagId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public TagDto GetTagById(int tagId)
    {
        var tag = _tagService.FindTagById(tagId);
        if (tag == null)
        {
            Response.StatusCode = 404;
            return null;
        }
        return TagDto.fromTag(tag);
    }

    [HttpGet("{tagId}/media")]
    [Authorize(Roles = UserRole.AllRoles)]
    public IEnumerable<MediaDto> GetMediasForTagWithId(int tagId)
    {
        var tag = _tagService.FindTagById(tagId);
        if (tag == null)
        {
            Response.StatusCode = 404;
            return null;
        }

        return tag.Medias.Select(MediaDto.fromMedia);
    }

    [HttpGet("self")]
    [Authorize(Roles = UserRole.AllRoles)]
    public IEnumerable<TagDto> GetAllTagsForUser()
    {
        var authorizedUserId = this.GetAuthorizedUserId();
        var loggedUser = _userService.FindUserById(authorizedUserId);
        return loggedUser?.Tags.Select(TagDto.fromTag) ?? new List<TagDto>();
    }

    [HttpPost]
    [Authorize(Roles = UserRole.AllRoles)]
    public TagDto CreateTag([FromBody] TagCreateRequest tagCreateRequest)
    {
        var currentlyLoggedUser = this.GetAuthorizedUserId();
        var loggedInUser = _userService.FindUserById(currentlyLoggedUser);
        if (loggedInUser == null)
        {
            throw new CodeException(UserErrorCode.UserNotExists, "Logged user does not exist");
        }

        var existingTag = loggedInUser.Tags.FirstOrDefault(x => x.Name == tagCreateRequest.TagName);
        if (existingTag != null)
        {
            return TagDto.fromTag(existingTag);
        }
        var newTag = new Tag
        {
            Medias = new List<Media>(),
            Name = tagCreateRequest.TagName,
            Owner = loggedInUser,
        };
        var savedTag = _tagService.Save(newTag);
        return TagDto.fromTag(savedTag);
    }


    [HttpDelete("{tagId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public void DeleteTagById(int tagId)
    {
        var authorizedUserId = this.GetAuthorizedUserId();
        var loggedUser = _userService.FindUserById(authorizedUserId);
        if (loggedUser == null)
        {
            throw new CodeException(UserErrorCode.UserNotExists);
        }

        var tagToDelete = _tagService.FindTagById(tagId);
        if (tagToDelete!.Owner == loggedUser || loggedUser.UserRole != UserRole.Admin)
        {
            _tagService.Delete(tagToDelete);
            Response.StatusCode = 204;
            return;
        }
        Response.StatusCode = 403;
    }
}