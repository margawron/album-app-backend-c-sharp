using System.ComponentModel;
using Album.Dtos.Domain;
using Album.Exceptions;
using Album.Extensions;
using Album.Models;
using Album.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Album.Controllers;

[Authorize]
[ApiController]
[Route("media")]
public class MediaController: ControllerBase
{
    private readonly MediaService _mediaService;
    private readonly UserService _userService;
    private readonly TagService _tagService;
    private readonly BinaryDataService _binaryDataService;
    public MediaController(MediaService mediaService, UserService userService, BinaryDataService binaryDataService, TagService tagService)
    {
        _mediaService = mediaService;
        _userService = userService;
        _binaryDataService = binaryDataService;
        _tagService = tagService;
    }

    [HttpGet("{mediaId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public MediaDto? GetMediaById(int mediaId)
    {
        var userId = int.Parse(this.ReadClaims()["id"].Value);
        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return null;
        }
        if (media.Owner.Id != userId)
        {
            Response.StatusCode = 403;
            return null;
        }
        return MediaDto.fromMedia(media);
    }
    
    [HttpGet("{mediaId}/tags")]
    [Authorize(Roles = UserRole.AllRoles)]
    public IEnumerable<TagDto> GetMediaTagsByMediaId(int mediaId)
    {
        var userId = int.Parse(this.ReadClaims()["id"].Value);
        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return null;
        }
        if (media.Owner.Id != userId)
        {
            Response.StatusCode = 403;
            return null;
        }
        return media.Tags.Select(TagDto.fromTag);
    }

    [HttpDelete("{mediaId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public void DeleteMediaById(int mediaId)
    {
        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return;
        }

        _mediaService.Delete(media);
    }

    [HttpGet("self")]
    [Authorize(Roles = UserRole.AllRoles)]
    public IEnumerable<MediaDto> GetAllMediaForLoggedUser()
    {
        var userId = this.GetAuthorizedUserId();
        var user = _userService.FindUserById(userId);
        
        return user?.Medias.Select(MediaDto.fromMedia) ?? new List<MediaDto>();
    }

    [HttpPost("upload")]
    [Authorize(Roles = UserRole.AllRoles)]
    public MediaDto UploadMedia(IFormFile file)
    {
        var userId = this.GetAuthorizedUserId();
        var memoryStream = new MemoryStream();
        file.CopyTo(memoryStream);
        var media = new Media
        {
            Tags = new List<Tag>(),
            MimeType = file.ContentType,
            Name = file.FileName,
            UserId = userId
        };
        var savedMedia = _mediaService.Create(media);
        var data = new BinaryData
        {
            Media = savedMedia,
            Content = memoryStream.GetBuffer(),
        };
        var savedData = _binaryDataService.Save(data);
        return MediaDto.fromMedia(savedMedia);
    }

    [HttpGet("binary/{mediaId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public FileResult GetMediaBinaryData(int mediaId)
    {
        var userId = this.GetAuthorizedUserId();
        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return null;
        }
        if (media.UserId != userId)
        {
            Response.StatusCode = 403;
            return null;
        }
        return new FileStreamResult(new MemoryStream(media.BinaryData.Content), media.MimeType);
    }

    [HttpPost("{mediaId}/tag/attach/{tagId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public MediaDto AttachTagToMedia(int mediaId, int tagId)
    {
        var authorizedUserId = this.GetAuthorizedUserId();
        var loggedUser = _userService.FindUserById(authorizedUserId);
        if (loggedUser == null)
        {
            throw new CodeException(UserErrorCode.UserNotExists);
        }

        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return null;
        }

        var tag = _tagService.FindTagById(tagId);
        if (tag == null)
        {
            Response.StatusCode = 404;
            return null;
        }

        if (media.Owner != loggedUser || tag.Owner != loggedUser)
        {
            Response.StatusCode = 403;
            return null;
        }

        if (media.Tags.FirstOrDefault(t => t.Id == tag.Id) != null)
        {
            return MediaDto.fromMedia(media);
        }
        
        media.Tags.Add(tag);
        var updated = _mediaService.Update(media);
        return MediaDto.fromMedia(updated);
    }


    [HttpPost("{mediaId}/tag/detach/{tagId}")]
    [Authorize(Roles = UserRole.AllRoles)]
    public MediaDto DetachTagToMedia(int mediaId, int tagId)
    {
        var authorizedUserId = this.GetAuthorizedUserId();
        var loggedUser = _userService.FindUserById(authorizedUserId);
        if (loggedUser == null)
        {
            throw new CodeException(UserErrorCode.UserNotExists);
        }

        var media = _mediaService.FindById(mediaId);
        if (media == null)
        {
            Response.StatusCode = 404;
            return null;
        }

        var tag = _tagService.FindTagById(tagId);
        if (tag == null)
        {
            Response.StatusCode = 404;
            return null;
        }

        if (media.Owner != loggedUser || tag.Owner != loggedUser)
        {
            Response.StatusCode = 403;
            return null;
        }
        if (media.Tags.FirstOrDefault(t => t.Id == tag.Id) == null)
        {
            return MediaDto.fromMedia(media);
        }
        
        media.Tags.Remove(tag);
        var updated = _mediaService.Update(media);
        return MediaDto.fromMedia(updated);
    }
}