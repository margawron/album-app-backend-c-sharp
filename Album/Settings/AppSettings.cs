namespace Album.Settings;

public class AppSettings
{
    public string Secret { get; set; }
    public int ExpirationTime { get; set; }
}