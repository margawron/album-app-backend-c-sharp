﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using NodaTime;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace Album.Migrations
{
    public partial class InitDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "User",
                columns: table => new
                {
                    usr_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    usr_username = table.Column<string>(type: "text", nullable: false),
                    usr_password_hash = table.Column<string>(type: "text", nullable: false),
                    usr_mail = table.Column<string>(type: "text", nullable: false),
                    usr_role = table.Column<string>(type: "text", nullable: false),
                    usr_creation_date = table.Column<Instant>(type: "timestamp with time zone", nullable: false),
                    usr_account_state = table.Column<int>(type: "integer", nullable: false),
                    usr_deactivation_date = table.Column<Instant>(type: "timestamp with time zone", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_User", x => x.usr_id);
                });

            migrationBuilder.CreateTable(
                name: "Media",
                columns: table => new
                {
                    mid_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    mid_mime_type = table.Column<string>(type: "text", nullable: false),
                    mid_name = table.Column<string>(type: "text", nullable: false),
                    mid_usr_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Media", x => x.mid_id);
                    table.ForeignKey(
                        name: "FK_Media_User_mid_usr_id",
                        column: x => x.mid_usr_id,
                        principalTable: "User",
                        principalColumn: "usr_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "Tag",
                columns: table => new
                {
                    tag_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    tag_name = table.Column<string>(type: "text", nullable: false),
                    tag_usr_id = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Tag", x => x.tag_id);
                    table.ForeignKey(
                        name: "FK_Tag_User_tag_usr_id",
                        column: x => x.tag_usr_id,
                        principalTable: "User",
                        principalColumn: "usr_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "BinaryData",
                columns: table => new
                {
                    bin_id = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    bin_mid_id = table.Column<int>(type: "integer", nullable: false),
                    bin_content = table.Column<byte[]>(type: "bytea", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BinaryData", x => x.bin_id);
                    table.ForeignKey(
                        name: "FK_BinaryData_Media_bin_mid_id",
                        column: x => x.bin_mid_id,
                        principalTable: "Media",
                        principalColumn: "mid_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MediaTag",
                columns: table => new
                {
                    MediasId = table.Column<int>(type: "integer", nullable: false),
                    TagsId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MediaTag", x => new { x.MediasId, x.TagsId });
                    table.ForeignKey(
                        name: "FK_MediaTag_Media_MediasId",
                        column: x => x.MediasId,
                        principalTable: "Media",
                        principalColumn: "mid_id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MediaTag_Tag_TagsId",
                        column: x => x.TagsId,
                        principalTable: "Tag",
                        principalColumn: "tag_id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_BinaryData_bin_mid_id",
                table: "BinaryData",
                column: "bin_mid_id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Media_mid_usr_id",
                table: "Media",
                column: "mid_usr_id");

            migrationBuilder.CreateIndex(
                name: "IX_MediaTag_TagsId",
                table: "MediaTag",
                column: "TagsId");

            migrationBuilder.CreateIndex(
                name: "IX_Tag_tag_usr_id",
                table: "Tag",
                column: "tag_usr_id");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "BinaryData");

            migrationBuilder.DropTable(
                name: "MediaTag");

            migrationBuilder.DropTable(
                name: "Media");

            migrationBuilder.DropTable(
                name: "Tag");

            migrationBuilder.DropTable(
                name: "User");
        }
    }
}
