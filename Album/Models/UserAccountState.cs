using Album.Exceptions;

namespace Album.Models;

public enum UserAccountState
{
    Normal,
    Suspended,
    Banned,
}

static class UserAccountStateExtensions
{
    public static String Mapping(this UserAccountState state)
    {
        return state switch
        {
            UserAccountState.Normal => "N",
            UserAccountState.Suspended => "S",
            UserAccountState.Banned => "B",
            _ => throw new InvalidEnumMappingException(typeof(UserAccountState), state)
        };
    }
}