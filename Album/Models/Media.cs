using System.ComponentModel.DataAnnotations.Schema;

namespace Album.Models;

public class Media
{
    [Column("mid_id")]
    public int Id { get; set; }
    [Column("mid_mime_type")]
    public string MimeType { get; set; }
    [Column("mid_name")]
    public string Name { get; set; }
    [Column("mid_usr_id")]
    public int UserId { get; set; }
    public virtual BinaryData BinaryData {get; set; }
    public virtual User Owner { get; set; }
    public virtual ICollection<Tag> Tags { get; set; }

}