using System.ComponentModel.DataAnnotations.Schema;

namespace Album.Models;

public class Tag
{
    [Column("tag_id")]
    public int Id { get; set; }
    [Column("tag_name")]
    public string Name { get; set; }
    [Column("tag_usr_id")]
    public int OwnerId { get; set; }
    
    public virtual User Owner { get; set; }
    public virtual ICollection<Media> Medias { get; set; }
}