using System.ComponentModel.DataAnnotations.Schema;

namespace Album.Models;

public class BinaryData
{
    
    [Column("bin_id")]
    public int Id { get; set; }   
    [Column("bin_mid_id")]
    public int MediaId { get; set; }
    [Column("bin_content")]
    public byte[] Content { get; set; }
    public virtual Media Media { get; set; }
}