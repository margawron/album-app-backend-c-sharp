using Album.Data;

namespace Album.Models.Repositories;

public class UserRepository
{
    private readonly ApplicationDbContext _context;

    public UserRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public User? FindByUsername(string username)
    {
        return _context.User.SingleOrDefault(user => user.Username == username);
    }

    public User? FindByEmail(string email)
    {
        return _context.User.SingleOrDefault(user => user.Email == email);
    }

    public User? FindUserById(int id)
    {
        return _context.User.Find(id);
    }

    public User Create(User user)
    {
        var entity = _context.User.Add(user);
        _context.SaveChanges();
        return entity.Entity;
    }

    public List<User> FindAll()
    {
        return _context.User.Select(x => x).ToList();
    }
}