using Album.Data;

namespace Album.Models.Repositories;

public class BinaryDataRepository
{
    private readonly ApplicationDbContext _context;

    public BinaryDataRepository(ApplicationDbContext context)
    {
        _context = context;
    }

    public BinaryData Save(BinaryData binaryData)
    {
        var entity = _context.Add(binaryData);
        _context.SaveChanges();
        return entity.Entity;
    }

    public BinaryData? FindById(int binaryDataId)
    {
        return _context.BinaryData.SingleOrDefault(x => x.Id == binaryDataId);
    }
}