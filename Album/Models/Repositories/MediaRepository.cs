using Album.Data;

namespace Album.Models.Repositories;

public class MediaRepository
{
    private readonly ApplicationDbContext _context;

    public MediaRepository(ApplicationDbContext ctx)
    {
        _context = ctx;
    }

    public Media? FindById(int mediaId)
    {
        return _context.Media.SingleOrDefault(media => media.Id == mediaId);
    }

    public Media Create(Media media)
    {
        var entity = _context.Media.Add(media);
        _context.SaveChanges();
        return entity.Entity;
    }

    public Media Update(Media media)
    {
        var entity = _context.Media.Update(media);
        _context.SaveChanges();
        return entity.Entity;
    }

    public void Delete(Media media)
    {
        _context.Media.Remove(media);
        _context.SaveChanges();
    }
}