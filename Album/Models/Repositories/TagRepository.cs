using Album.Data;

namespace Album.Models.Repositories;

public class TagRepository
{
    private readonly ApplicationDbContext _context;

    public TagRepository(ApplicationDbContext ctx)
    {
        _context = ctx;
    }
    
    public Tag? FindById(int tagId)
    {
        return _context.Tag.SingleOrDefault(tag => tag.Id == tagId);
    }

    public Tag Save(Tag tag)
    {
        var entity = _context.Tag.Add(tag);
        _context.SaveChanges();
        return entity.Entity;
    }

    public void Delete(Tag tag)
    {
        _context.Tag.Remove(tag);
        _context.SaveChanges();
    }
}