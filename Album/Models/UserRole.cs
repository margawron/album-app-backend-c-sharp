using Album.Exceptions;

namespace Album.Models;

public class UserRole
{
    public const string Admin = "Admin";
    public const string User = "User";

    public const string AllRoles = $"{User}, {Admin}";
}