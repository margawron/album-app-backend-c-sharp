using System.ComponentModel.DataAnnotations.Schema;
using NodaTime;

namespace Album.Models;


public class User
{
    [Column("usr_id")]
    public int Id { get; set; }
    
    [Column("usr_username")]
    public String Username { get; set; }
    
    [Column("usr_password_hash")]
    public String PasswordHash { get; set; }
    
    [Column("usr_mail")]
    public String Email { get; set; }
    
    [Column("usr_role")]
    public string UserRole { get; set; }
    
    [Column("usr_creation_date")]
    public Instant CreationDate { get; set; }

    [Column("usr_account_state")] 
    public UserAccountState AccountState { get; set; }
    
    [Column("usr_deactivation_date")]
    public Instant? DeactivationDate { get; set; }
    
    public virtual ICollection<Media> Medias { get; set; }
    public virtual ICollection<Tag> Tags { get; set; }
}