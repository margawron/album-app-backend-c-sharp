using System.Security.Claims;
using Microsoft.AspNetCore.Mvc;

namespace Album.Extensions;

public static class ClaimsExtension
{
    public static Dictionary<string, Claim> ReadClaims(this ControllerBase controllerBase)
    {
        var dict = new Dictionary<string, Claim>();
        foreach (var userClaim in controllerBase.User.Claims)
        {
            dict.Add(userClaim.Type, userClaim);
        }

        return dict;
    }

    public static int GetAuthorizedUserId(this ControllerBase controllerBase)
    {
        return int.Parse(controllerBase.ReadClaims()["id"].Value);
    }

}