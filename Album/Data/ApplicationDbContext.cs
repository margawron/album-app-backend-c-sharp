using Album.Models;
using Microsoft.EntityFrameworkCore;

namespace Album.Data;

public class ApplicationDbContext : DbContext
{
    public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
    {
    }


    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        base.OnModelCreating(modelBuilder);

        modelBuilder.Entity<User>()
            .HasMany(u => u.Tags)
            .WithOne(t => t.Owner)
            .OnDelete(DeleteBehavior.Cascade);
        modelBuilder.Entity<User>()
            .HasMany(u => u.Medias)
            .WithOne(m => m.Owner)
            .OnDelete(DeleteBehavior.Cascade);

        modelBuilder.Entity<BinaryData>()
            .HasOne(b => b.Media)
            .WithOne(m => m.BinaryData)
            .OnDelete(DeleteBehavior.Cascade);
        
        modelBuilder.Entity<Tag>()
            .HasMany(t => t.Medias)
            .WithMany(m => m.Tags);
    }

    public DbSet<User> User { get; set; }
    public DbSet<Media> Media { get; set; }
    public DbSet<Tag> Tag { get; set; }
    public DbSet<BinaryData> BinaryData { get; set; }
}