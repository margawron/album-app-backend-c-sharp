using System.Net;
using Album.Dtos;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Album.Exceptions;

public class CodeExceptionFilter : ExceptionFilterAttribute
{
    public override void OnException(ExceptionContext context)
    {
        if (context.Exception is CodeException exception)
        {
            context.Result = new JsonResult(new ErrorDto
            {
                ErrorCode = exception.Code.GetErrorCode(),
                ErrorMessage = exception.Message,
            });
        }
        context.HttpContext.Response.ContentType = "application/json";
        context.HttpContext.Response.StatusCode = (int) HttpStatusCode.BadRequest;
    }
}