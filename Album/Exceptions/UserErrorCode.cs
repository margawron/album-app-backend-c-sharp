namespace Album.Exceptions;

public class UserErrorCode : ErrorCode
{
    public static readonly UserErrorCode InvalidPassword = new("user.password.invalid");
    public static readonly UserErrorCode UserExists = new("user.exists");
    public static readonly UserErrorCode UserNotExists = new("user.notExists");

    private String ErrorCode;
    public UserErrorCode(string errorCode)
    {
        ErrorCode = errorCode;
    }
    
    public string GetErrorCode()
    {
        return ErrorCode;
    }
}