namespace Album.Exceptions;

public class CodeException : Exception
{
    public ErrorCode Code { get; }

    public CodeException(ErrorCode code, String message = "") :base(message)
    {
        Code = code;
    }
}