namespace Album.Exceptions;

public interface ErrorCode
{
    public string GetErrorCode();
}