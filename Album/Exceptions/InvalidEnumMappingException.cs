namespace Album.Exceptions;

public class InvalidEnumMappingException : Exception
{
    public Type EnumType { get; }
    public Enum EnumValue { get; }
    public InvalidEnumMappingException(Type enumType, Enum enumValue)
    {
        EnumType = enumType;
        EnumValue = enumValue;
    }

    public override string Message => $"Invalid value: ${EnumValue} for EnumType: ${EnumType}";
}